import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Security, SecureRoute, ImplicitCallback } from "@okta/okta-react";

import Navbar from "./components/layout/Navbar";
import Home from "./components/pages/Home";
import Staff from "./components/pages/Staff";
import Login from './components/auth/Login';


const config = {
  issuer: "https://dev-898577.oktapreview.com/oauth2/default",
  redirect_uri: window.location.origin + "/implicit/callback",
  client_id: "0oafi5uy47CJijkAw0h7"
};
function onAuthRequired({ history }) {
  history.push("/login");
}

export default class App extends Component {
  render() {
    return (
      <Router>
        <Security
          issuer={config.issuer}
          client_id={config.client_id}
          redirect_uri={config.redirect_uri}
          onAuthRequired={onAuthRequired}
        ><Navbar />
          <div>
            
            <div className="container jumbotron">
              <Route path="/" exact={true} component={Home} />
              <Route
                path="/login"
                exact={true}
                render={() => (
                  <Login baseUrl="https://dev-898577.oktapreview.com" />
                )}
              />
              <Route path="/implicit/callback" component={ImplicitCallback}/>
              <SecureRoute path="/staff" exact={true} component={Staff} />
            </div>
          </div>
        </Security>
      </Router>
    );
  }
}
