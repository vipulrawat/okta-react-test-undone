import React, { Component } from "react";
import { withAuth } from "@okta/okta-react";
import { Link } from "react-router-dom";

export default withAuth(
  class Home extends Component {
    constructor(props) {
      super(props);
      this.state = { authenticated: null };
      this.checkAuthentication();
    }

    checkAuthentication = async () => {
      const authenticated = await this.props.auth.isAuthenticated();
      if (authenticated !== this.state.authenticated) {
        this.setState({ authenticated });
      }
    };

    componentDidUpdate() {
      this.checkAuthentication();
    }

    login = async() => {
      // Redirect to '/' after login
      this.props.login("/login");
    };

    logout = async() => {
      // Redirect to '/' after logout
      this.props.auth.logout("/");
    };

    render() {
      if (this.state.authenticated === null) return null;
      const mainContent = this.state.authenticated ? (
        <div>
          <p className="lead">
            You have entered staff <Link to="/staff">Click here</Link>
          </p>
          <button className="btn btn-light btn-lg" onClick={this.logout}>
            Logout
          </button>
        </div>
      ) : (
        <div>
          <p className="lead">
            You are staff <Link to="/staff">Click here</Link>
          </p>
          <button className="btn btn-dark btn-lg" onClick={this.login}>
            Login
          </button>
        </div>
      );
      return (
        <div className="jumbotron">
          <h1 className="display-4">Welcome Portal</h1>
          {mainContent}
        </div>
      );
    }
  }
);
